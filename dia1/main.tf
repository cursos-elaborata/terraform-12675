terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
    aws = {
      source = "hashicorp/aws"
      version = "4.5.0"
    }
  }
}

provider "random" {
  # Configuration options
}

provider "aws" {
  region = "us-east-1"
}

variable "imagem" {
  description = "Define a imagem a ser utilizada na instancia"
  default     = "ami-04505e74c0741db8d"
}

variable "tipo_instancia" {
  description = "Define o tamanho da instancia"
}

variable "aplicacao" {
  description = "Define o nome da aplicação"
}

resource "random_pet" "pet" {
  
}

data "cloudinit_config" "userdata" {
  gzip = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/userdata.sh.tpl", {
      pet = random_pet.pet.id
      })
    filename = "userdata.sh"
  }
}

resource "aws_instance" "ec2-turma-12675" {
  ami                    = var.imagem
  instance_type          = var.tipo_instancia
  vpc_security_group_ids = [aws_security_group.sg-turma-12675.id]
  key_name               = aws_key_pair.kp-turma-12675.key_name
  user_data              = data.cloudinit_config.userdata.rendered

  tags = {
    Name = random_pet.pet.id
    Application = var.aplicacao
  }
}

resource "aws_security_group" "sg-turma-12675" {
  name = "sgturma12675"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Application = var.aplicacao
  }

}

resource "aws_key_pair" "kp-turma-12675" {
  key_name   = "turma-12675"
  public_key = file("~/.ssh/id_rsa.pub") 

  tags = {
    Application = var.aplicacao
  }
}




terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.5.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "nomes" {
    description = "Lista de Nomes"
    type = list(string)
    default = ["danilo", "jefferson", "william"]
}

output "nomes_em_maiuscula" {
  value = [for nome in var.nomes : upper(nome)]
}

variable "chave_valor" {
  type = map(string)
  default = {
    "chave1" = "valor1"
    "chave2" = "valor2"
    "chave3" = "valor3"
  }
}

variable "lista_plantonista" {
  type = map(string)
  default = {
    "plantao1" = "danilo"
    "plantao2" = "jefferson"
    "plantao3" = "william"
  }
}

output "saida_chave_valor" {
  value = [for chave, valor in var.chave_valor : "${chave} tem o valor de ${valor}"]
}

resource "aws_instance" "ec2-turma-12675-for" {  
  ami                    = "ami-04505e74c0741db8d"
  instance_type          = "t2.micro"

  tags = {for chave, valor in var.chave_valor : upper(chave) => upper(valor)}
}

resource "aws_instance" "ec2-turma-12675-for-2" {  
  ami                    = "ami-04505e74c0741db8d"
  instance_type          = "t2.micro"

  tags = {for plantao, nome in var.lista_plantonista : plantao => nome if substr(nome, 0, 1) != "j"}
}
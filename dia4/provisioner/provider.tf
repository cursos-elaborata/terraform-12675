terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
    aws = {
      source = "hashicorp/aws"
      version = "4.5.0"
    }
  }
}

provider "random" {
  # Configuration options
}

provider "aws" {
  region = "us-east-1"
}
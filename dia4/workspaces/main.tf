resource "random_pet" "pet" {
  
}

data "cloudinit_config" "userdata" {
  gzip = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/userdata.sh.tpl", {
      pet = random_pet.pet.id
      })
    filename = "userdata.sh"
  }
}

resource "aws_instance" "ec2-turma-12675" {
  # Cria um vetor/lista de instâncias estáticas com tamanho definido
  count                  = terraform.workspace == "prd" ? 2 : 1 
  ami                    = var.imagem
  instance_type          = var.tipo_instancia
  vpc_security_group_ids = [aws_security_group.sg-turma-12675.id]
  key_name               = terraform.workspace == "prd" ? "" : aws_key_pair.kp-turma-12675[0].key_name
  user_data              = data.cloudinit_config.userdata.rendered

  tags = {
    Name = random_pet.pet.id
    Application = var.aplicacao
  }
}

resource "aws_security_group" "sg-turma-12675" {
  name = "sgturma12675-${terraform.workspace}"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Application = var.aplicacao
  }

}

resource "aws_key_pair" "kp-turma-12675" {
  count = terraform.workspace == "prd" ? 0 : 1
  key_name   = "turma-12675-${terraform.workspace}"
  public_key = file("~/.ssh/id_rsa.pub") 

  tags = {
    Application = var.aplicacao
  }
}
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.5.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "nomes" {
    description = "Lista de Nomes"
    type = list(string)
    default = ["danilo", "jefferson", "william", "danilo", "thiago", "kleber"]
}

resource "aws_iam_user" "name" {
  for_each = toset(var.nomes)
  name = each.value
}

resource "aws_instance" "ec2-turma-12675-for-2" {  
  count                  = length(var.nomes)
  ami                    = "ami-04505e74c0741db8d"
  instance_type          = "t2.micro"

  tags = {
      "Name" = "instancia${count.index + 1}"
  }
}


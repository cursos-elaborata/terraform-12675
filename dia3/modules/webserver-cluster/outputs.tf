output "alb_dns_name" {
  value       = aws_lb.lb-t12675.dns_name
  description = "DNS do Load Balancer"
}
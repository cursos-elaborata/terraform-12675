module "dynamodb-table" {
  source  = "terraform-aws-modules/dynamodb-table/aws"

  hash_key = "t12675-id"
  name     = "t12675"
  billing_mode = "PAY_PER_REQUEST"
  attributes = [{
      name = "t12675-id"
      type = "N"
  }]
  # insert the 7 required variables here
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "6.8.0"
  # insert the 4 required variables here
  name = "t12675"
#   name_prefix = "abcde"
  subnets = data.aws_subnet_ids.default.ids
}

module "hello_world" {
    source = "github.com/cloudposse/terraform-example-module.git"
    example = "Olá, Mundo"
}



output "nome_load_balancer" {
  value       = module.webserver_cluster.alb_dns_name
  description = "Nome do Load Balancer"
}
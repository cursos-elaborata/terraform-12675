terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.5.0"
    }
  }
}

terraform {
    backend "s3" {
        bucket         = "turma12675-state"
        key            = "terraform-module.tfstate"
        region         = "us-east-2"
        dynamodb_table = "turma12675-lock"
        encrypt        = true 
    }    
}

provider "aws" {
  region = var.region
}


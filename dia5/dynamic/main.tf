resource "random_pet" "pet" {
  
}

data "cloudinit_config" "userdata" {
  gzip = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/userdata.sh.tpl", {
      pet = random_pet.pet.id
      })
    filename = "userdata.sh"
  }
}

resource "aws_instance" "ec2-turma-12675" {
  ami                    = var.imagem
  instance_type          = var.tipo_instancia
  vpc_security_group_ids = [aws_security_group.sg-turma-12675.id]
  key_name               = aws_key_pair.kp-turma-12675.key_name
  user_data              = data.cloudinit_config.userdata.rendered

  tags = {
    Name = random_pet.pet.id
    Application = var.aplicacao
  }



}

resource "aws_security_group" "sg-turma-12675" {
  name = "sgturma12675-dynamic"

  dynamic "ingress" {
    for_each = var.regras_ingress
    content {
      description = ingress.value.descricao
      from_port = ingress.value.porta
      to_port = ingress.value.porta
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

}

resource "aws_key_pair" "kp-turma-12675" {
  key_name   = "turma-12675-dynamic"
  public_key = file("~/.ssh/id_rsa.pub") 

  tags = {
    Application = var.aplicacao
  }
}

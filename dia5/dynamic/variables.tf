variable "imagem" {
  description = "Define a imagem a ser utilizada na instancia"
  default     = "ami-04505e74c0741db8d"
}

variable "tipo_instancia" {
  description = "Define o tamanho da instancia"
  default = "t3.micro"
}

variable "aplicacao" {
  description = "Define o nome da aplicação"
  default = "xpto"
}

variable "regras_ingress" {
  type = list(any)
  default = [{
    porta = 80
    descricao = "Regra do ingress porta 80"
  },
  {
    porta = 443
    descricao = "Regra do ingress porta 443"
  },
  {
    porta = 8080
    descricao = "Regra do ingress porta 8080"
  },
  {
    porta = 22
    descricao = "Regra do ingress porta 22"
  }
  ]
}
module "webserver_cluster" {
    source                       = "../modules/webserver-cluster"

    instance_type                = var.instance_type
    mensagem                     = var.mensagem
    alb_name                     = var.alb_name
    instance_security_group_name = var.instance_security_group_name
    alb_security_group_name      = var.alb_security_group_name
    region                       = var.region
}


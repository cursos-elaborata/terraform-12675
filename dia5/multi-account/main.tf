resource "random_pet" "pet" {
  
}

data "aws_ami" "ubuntu-dev" {
  provider = aws.dev
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_ami" "ubuntu-hml" {
  provider = aws.hml
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "cloudinit_config" "userdata" {
  gzip = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/userdata.sh.tpl", {
      pet = random_pet.pet.id
      })
    filename = "userdata.sh"
  }
}

resource "aws_instance" "ec2-turma-12675" {
  provider               = aws.hml
  ami                    = data.aws_ami.ubuntu-hml.id
  instance_type          = var.tipo_instancia
  vpc_security_group_ids = [aws_security_group.sg-turma-12675.id]
  key_name               = aws_key_pair.kp-turma-12675.key_name
  user_data              = data.cloudinit_config.userdata.rendered

  tags = {
    Name = random_pet.pet.id
    Application = var.aplicacao
  }

}

resource "aws_instance" "ec2-turma-dev" {
  provider               = aws.dev
  ami                    = data.aws_ami.ubuntu-dev.id
  instance_type          = var.tipo_instancia
  key_name               = aws_key_pair.kp-turma-12675-dev.key_name
}

resource "aws_security_group" "sg-turma-12675" {
  provider = aws.hml
  name = "sgturma12675-multi"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Application = var.aplicacao
  }

}

resource "aws_key_pair" "kp-turma-12675" {
  provider = aws.hml
  key_name   = "turma-12675-hml"
  public_key = file("~/.ssh/id_rsa.pub") 

  tags = {
    Application = var.aplicacao
  }
}

resource "aws_key_pair" "kp-turma-12675-dev" {
  provider = aws.dev
  key_name   = "turma-12675-dev"
  public_key = file("~/.ssh/id_rsa.pub") 

  tags = {
    Application = var.aplicacao
  }
}

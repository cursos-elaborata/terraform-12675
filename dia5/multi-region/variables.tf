variable "imagem" {
  description = "Define a imagem a ser utilizada na instancia"
  default     = "ami-04505e74c0741db8d"
}

variable "tipo_instancia" {
  description = "Define o tamanho da instancia"
  default = "t3.micro"
}

variable "aplicacao" {
  description = "Define o nome da aplicação"
  default = "xpto"
}
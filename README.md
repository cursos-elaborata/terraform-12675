# Terraform 12675

## Dia 1

- O que é o Terraform?
- Que problema ele resolve
- HCL (Hashicorp Configuration Language)
- Estrutura

## Dia 2

- Gerenciamento de Estado
- Workspaces

## Dia 3

- Módulos

## Dia 4

- Count
- For Each
- Local
- Outras Expressões HCL

## Dia 5

- Dynamic
- Provisioner
